<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Membuat role admin 
        $adminrole = new Role();
        $adminrole->name = "admin";
        $adminrole->display_name = "Admin";
        $adminrole->save();
        //Membuat role member
        $memberrole = new Role();
        $memberrole->name = "member";
        $memberrole->display_name = "Member";
        $memberrole->save();

        //Membuat sample admin 
        $admin = new User();
        $admin->name = 'Admin Larapus';
        $admin->email = 'admin@gmail.com';
        $admin->password = bcrypt('rahasia');
        $admin->is_verified= 1;
        $admin->save();
        $admin->attachRole($adminrole);

        //Membuat sample member
        $member = new User();
        $member->name = "Sample Member";
        $member->email = 'member@gmail.com';
        $member->password = bcrypt('rahasia');
        $member->is_verified=1;
        $member->save();
        $member->attachRole($memberrole);
    }
}
